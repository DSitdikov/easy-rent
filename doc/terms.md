#### What's a Cocopods?

CocoaPods is a dependency manager for Swift and Objective-C Cocoa projects.
Includes lock file, target iOS version, etc. It is added to automatically
after installing additional libraries such this: `url_launcher`.

It includes:
1. `Podfile` \
The Podfile is a specification that describes the dependencies of the targets of one or more Xcode projects.

2. `Podfile.lock` \
This file is generated after the first run of pod install, and tracks the version of each Pod that was installed.

#### What're a target, a workspace and a build configuration of iOS project?

https://habr.com/ru/company/redmadrobot/blog/313706/
