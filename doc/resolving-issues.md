#### Debug.xcconfig line 1: Unable to find included file "Pods/Target Support Files/Pods-Runner/Pods-Runner.debug.xcconfig"

1. `cd ios`
2. `pod install`

#### Error linking additional libraries in Xcode project

If your additional libraries use `Cocopods`, just open the `*.xcworkspace` instead of `*.xcodeproj`.

#### Waiting for another flutter command to release the startup lock

Just run a `killall -9 dart` and restart application. Also you can try: `flutter clean`.

### Bad state: Too many elements test

The problem here is that you assigned key to two different widgets: as for parent, as for child, 
This causes flutter driver to find two widgets instead of one.
