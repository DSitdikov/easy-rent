### Unit widget tests

Issues:
- There is a issue with not working characters. There are white boxes instead of fonts symbols.

Experience:
1. It can get snapshots, too, but if at least one pixel changed - it fails. Also we can't see 
differences between images.
 
Related articles:
1. [Do you see the difference flutter golden files?](https://medium.com/flutter-community/do-you-see-the-difference-flutter-golden-files-a1d431ed8a69)
   
### E2E Tests example

Experience:
1. Running one single scenario takes time: nearly 25-30s
2. It works great
3. We have got to create instrument by ourselves for get differences between snapshots. Now it returns
   only screenshots

Related articles:
1. [Testing Flutter UI with flutter driver](https://medium.com/flutter-community/testing-flutter-ui-with-flutter-driver-c1583681e337)

### Performance Testing

https://medium.com/flutter/performance-testing-of-flutter-apps-df7669bb7df7
