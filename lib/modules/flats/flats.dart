import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import './cards-stack/cards-stack.dart';
import './data.dart';
import 'no-data/no-data.dart';

class FlatsModule extends StatefulWidget {
  const FlatsModule({this.title});

  final String title;

  @override
  FlatsModuleState createState() => FlatsModuleState();
}

class FlatsModuleState extends State<FlatsModule> with TickerProviderStateMixin {
  AnimationController _animationController;

  List<List<DecorationImage>> data = flats;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(milliseconds: 1000),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = .4;

    return Stack(children: <Widget>[
      Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: const AssetImage('assets/background.png'),
            fit: BoxFit.cover,
          ),
        ),
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          alignment: Alignment.center,
          child: data.isNotEmpty
              ? CardsStack(
                  data: data,
                  animationController: _animationController,
                  onSwipe: updateData,
                )
              : NoData(),
        ),
      ),
    ]);
  }

  void updateData(List<List<DecorationImage>> value) {
    setState(() {
      data = value;
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
