import 'package:flutter/material.dart';
import '../../card/common/card-content.dart';

class UnderlyingCard extends StatelessWidget {
  const UnderlyingCard({this.images, this.offsetBottom, this.width, Key key}) : super(key: key);

  final double offsetBottom;
  final double width;
  final List<DecorationImage> images;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 100.0 + offsetBottom,
      child: CardContent(
        cardWidth: width,
        images: images,
      ),
    );
  }
}
