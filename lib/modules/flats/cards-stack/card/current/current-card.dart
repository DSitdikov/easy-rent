import 'package:flutter/material.dart';
import '../../../../../helpers/helpers.dart';
import '../../../details/details.dart';
import '../../card/common/card-content.dart';
import '../../enums.dart';

class CurrentCard extends StatelessWidget {
  const CurrentCard({
    Key key,
    this.index,
    this.images,
    this.bottom,
    this.right,
    this.cardWidth,
    this.rotation,
    this.skew,
    this.context,
    this.currentSwipeAnimationType,
    this.onDislike,
    this.onLike,
    this.onDislikePress,
    this.onLikePress
  }) : super(key: key);

  final List<DecorationImage> images;
  final double bottom;
  final double right;

  final double cardWidth;
  final double rotation;
  final double skew;
  final int index;
  final BuildContext context;
  final Function onDislike;
  final SwipeType currentSwipeAnimationType;
  final Function onLike;
  final Function onLikePress;
  final Function onDislikePress;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 100.0 + bottom,
      right: currentSwipeAnimationType == SwipeType.RIGHT ? right != 0.0 ? right : null : null,
      left: currentSwipeAnimationType == SwipeType.LEFT ? right != 0.0 ? right : null : null,
      child: Dismissible(
        key: UniqueKey(),
        crossAxisEndOffset: -0.3,
        onResize: noop,
        onDismissed: (DismissDirection direction) {
          if (direction == DismissDirection.endToStart) {
            onDislike(images);
          } else {
            onLike(images);
          }
        },
        child: Transform(
          alignment: currentSwipeAnimationType == SwipeType.RIGHT ? Alignment.bottomRight : Alignment.bottomLeft,
          transform: Matrix4.skewX(skew),
          child: RotationTransition(
            turns: AlwaysStoppedAnimation<double>(
              currentSwipeAnimationType == SwipeType.RIGHT ? rotation / 360 : -rotation / 360,
            ),
            child: Hero(
              key: const Key('DetailsLink'),
              tag: 'img',
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(PageRouteBuilder<Details>(
                    pageBuilder: (_, __, ___) =>
                        Details(
                          images: images,
                          onLikePress: noop,
                          onDislikePress: noop,
                        ),
                  ));
                },
                child: CardContent(
                  index: index,
                  cardWidth: cardWidth,
                  images: images,
                  isGalleryModeOn: true,
                  onLikePress: onLikePress,
                  onDislikePress: onDislikePress,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
