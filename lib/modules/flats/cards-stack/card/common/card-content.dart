import 'package:easy_rent/modules/flats/common/frosted-container/frosted-container.dart';
import 'package:flutter/material.dart';
import 'package:easy_rent/common/theme.dart';
import 'actions/card-actions.dart';
import 'image/card-image.dart';

class CardContent extends StatelessWidget {
  const CardContent({
    this.cardWidth,
    this.images,
    this.isGalleryModeOn = false,
    this.onLikePress,
    this.onDislikePress,
    this.index,
  });

  final double cardWidth;
  final int index;
  final List<DecorationImage> images;
  final Function onLikePress;
  final Function onDislikePress;
  final bool isGalleryModeOn;

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Card(
      color: Colors.transparent,
      elevation: 4.0,
      child: Container(
        alignment: Alignment.center,
        width: screenSize.width / 1.2 + cardWidth,
        height: screenSize.height / 1.7,
        decoration: BoxDecoration(
          color: AppTheme.primaryColor,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                CardImage(
                  screenSize: screenSize,
                  cardWidth: cardWidth,
                  images: images,
                  isGalleryModeOn: isGalleryModeOn,
                ),
                CardActions(
                  index: index,
                  screenSize: screenSize,
                  cardWidth: cardWidth,
                  onLikePress: onLikePress,
                  onDislikePress: onDislikePress,
                )
              ],
            ),
            Positioned(
              top: 20.0,
              left: 20.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(40.0),
                child: FrostedContainer(
                  child: Container(
                    padding: const EdgeInsets.only(
                      top: 10.0,
                      bottom: 10.0,
                      left: 14.0,
                      right: 14.0,
                    ),
                    child: Center(
                      child: Row(
                        children: <Widget>[
                          Text(
                            '₽ 15 т. ',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            '/мес',
                            style: TextStyle(fontSize: 14.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
