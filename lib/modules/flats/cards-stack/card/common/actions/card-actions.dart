import 'package:easy_rent/modules/flats/common/btn/card-btn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:easy_rent/common/theme.dart';

class CardActions extends StatelessWidget {
  const CardActions({
    this.index,
    this.screenSize,
    this.cardWidth,
    this.onDislikePress,
    this.onLikePress,
  });

  final Size screenSize;
  final int index;
  final double cardWidth;
  final Function onLikePress;
  final Function onDislikePress;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenSize.width / 1.2 + cardWidth,
      height: screenSize.height / 1.7 - screenSize.height / 2.2,
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          CardBtn(
            title: 'Нет',
            color: AppTheme.dislikeColor,
            onPressed: onDislikePress,
          ),
          CardBtn(
            title: 'Да',
            key: ValueKey<String>('YesButton ' + index.toString()),
            color: AppTheme.likeColor,
            onPressed: onLikePress,
          ),
        ],
      ),
    );
  }
}
