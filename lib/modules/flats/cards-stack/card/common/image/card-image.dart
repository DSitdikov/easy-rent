import 'package:easy_rent/modules/flats/common/carousel/carousel.dart';
import 'package:flutter/widgets.dart';

class CardImage extends StatelessWidget {
  const CardImage({
    this.screenSize,
    this.cardWidth,
    this.images,
    this.isGalleryModeOn = false,
  });

  final Size screenSize;
  final double cardWidth;
  final bool isGalleryModeOn;
  final List<DecorationImage> images;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenSize.width / 1.2 + cardWidth,
      height: screenSize.height / 2.2,
      decoration: !isGalleryModeOn ? getBackground() : null,
      child: isGalleryModeOn ? Carousel(images: images) : null,
    );
  }

  BoxDecoration getBackground() {
    return BoxDecoration(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(8.0),
        topRight: Radius.circular(8.0),
      ),
      image: images[0],
    );
  }
}
