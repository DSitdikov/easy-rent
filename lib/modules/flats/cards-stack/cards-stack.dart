import 'package:easy_rent/helpers/helpers.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import './../cards-stack/card/current/current-card.dart';
import './../cards-stack/card/underlying/underlying-card.dart';
import 'enums.dart';

class CardsStack extends StatefulWidget {
  const CardsStack({this.data, this.animationController, this.onSwipe = noop});

  final List<List<DecorationImage>> data;
  final AnimationController animationController;
  final Function onSwipe;

  @override
  CardsStackState createState() => CardsStackState();
}

class CardsStackState extends State<CardsStack> {
  static const int NUMBER_OF_SHOWING_CARDS = 4;

  SwipeType currentSwipeAnimationType = SwipeType.RIGHT;

  Animation<double> rotate;
  Animation<double> right;
  Animation<double> bottom;
  Animation<double> width;

  @override
  void initState() {
    super.initState();

    rotate = Tween<double>(begin: -0.0, end: -40.0).animate(
      CurvedAnimation(parent: widget.animationController, curve: Curves.ease),
    );

    rotate.addListener(() {
      setState(() {
        if (!rotate.isCompleted) {
          return;
        }

        final List<DecorationImage> lastImage = widget.data.removeLast();
        widget.data.insert(0, lastImage);
        widget.animationController.reset();
      });
    });

    right = Tween<double>(begin: 0.0, end: 400.0).animate(
      CurvedAnimation(parent: widget.animationController, curve: Curves.ease),
    );

    width = Tween<double>(begin: 20.0, end: 25.0).animate(
      CurvedAnimation(parent: widget.animationController, curve: Curves.bounceOut),
    );
  }

  @override
  Widget build(BuildContext context) {
    final List<List<DecorationImage>> subItems =
        NUMBER_OF_SHOWING_CARDS < widget.data.length ? widget.data.sublist(0, NUMBER_OF_SHOWING_CARDS) : widget.data;
    final List<List<DecorationImage>> subItemsToDisplay = subItems.reversed.toList();

    const double currentCardBottomPosition = 30.0;
    const double currentCardWidth = 40.0;
    const double widthOffset = 10.0;
    const double verticalOffset = 8.0;

    return Stack(
      alignment: AlignmentDirectional.center,
      children: subItemsToDisplay
          .asMap()
          .map((int i, final List<DecorationImage> flatImages) {
            final bool isNotLastItem = subItemsToDisplay.indexOf(flatImages) < subItemsToDisplay.length - 1;
            return MapEntry<int, Widget>(
              i,
              isNotLastItem
                  ? UnderlyingCard(
                      key: Key('Card' + i.toString()),
                      images: flatImages,
                      offsetBottom: currentCardBottomPosition + (subItemsToDisplay.length - 1 - i) * verticalOffset,
                      width: currentCardWidth - (subItemsToDisplay.length - 1 - i) * widthOffset,
                    )
                  : CurrentCard(
                      index: i,
                      key: Key('Card' + i.toString()),
                      images: flatImages,
                      bottom: currentCardBottomPosition,
                      right: right.value,
                      cardWidth: currentCardWidth,
                      rotation: rotate.value,
                      skew: rotate.value < -10 ? .1 : .0,
                      context: context,
                      onDislike: dismissImg,
                      currentSwipeAnimationType: currentSwipeAnimationType,
                      onLike: addImg,
                      onLikePress: swipeRight,
                      onDislikePress: swipeLeft,
                    ),
            );
          })
          .values
          .toList(),
    );
  }

  void addImg(List<DecorationImage> img) {
    setState(() {
      widget.data.remove(img);
    });
  }

  void dismissImg(List<DecorationImage> img) {
    setState(() {
      widget.data.remove(img);
    });
  }

  void swipeRight() {
    if (currentSwipeAnimationType == SwipeType.RIGHT) {
      setState(() {
        currentSwipeAnimationType = SwipeType.LEFT;
      });
    }

    widget.onSwipe(widget.data);

    launch('tel://89650367078');

    _startSwipeAnimation();
  }

  void swipeLeft() {
    if (currentSwipeAnimationType == SwipeType.LEFT) {
      setState(() {
        currentSwipeAnimationType = SwipeType.RIGHT;
      });
    }

    widget.onSwipe(widget.data);

    _startSwipeAnimation();
  }

  Future<void> _startSwipeAnimation() async {
    try {
      await widget.animationController.forward();
    } on TickerCanceled {
      noop();
    }
  }
}
