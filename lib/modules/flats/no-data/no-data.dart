import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NoData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text('No Event Left', style: TextStyle(color: Colors.white, fontSize: 50.0));
  }
}
