import 'package:flutter/widgets.dart';

List<List<DecorationImage>> flats = <List<DecorationImage>>[
  <DecorationImage>[
    DecorationImage(image: ExactAssetImage('assets/flats/1/1.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/1/2.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/1/3.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/1/4.jpg'), fit: BoxFit.cover),
  ],
  <DecorationImage>[
    DecorationImage(image: ExactAssetImage('assets/flats/2/1.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/2/2.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/2/3.jpg'), fit: BoxFit.cover),
  ],
  <DecorationImage>[
    DecorationImage(image: ExactAssetImage('assets/flats/3/1.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/3/2.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/3/3.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/3/4.jpg'), fit: BoxFit.cover),
  ],
  <DecorationImage>[
    DecorationImage(image: ExactAssetImage('assets/flats/4/1.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/4/2.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/4/3.jpg'), fit: BoxFit.cover),
    DecorationImage(image: ExactAssetImage('assets/flats/4/4.jpg'), fit: BoxFit.cover),
  ],
];
