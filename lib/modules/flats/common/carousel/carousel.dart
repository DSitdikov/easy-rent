import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Carousel extends StatefulWidget {
  const Carousel({this.images = const <DecorationImage>[]});

  final List<DecorationImage> images;

  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  @override
  Widget build(BuildContext context) {
    return PageView(
      children: widget.images.map<Widget>(
        (DecorationImage image) => Container(
          decoration: BoxDecoration(image: image),
        ),
      ).toList(),
    );
  }
}
