import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../../../helpers/helpers.dart';

class CardBtn extends StatelessWidget {
  const CardBtn({
    this.title = '',
    this.color,
    this.key,
    this.onPressed = noop,
  });

  final String title;
  final Color color;
  final Function onPressed;

  @override
  // ignore: overridden_fields
  final Key key;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: const EdgeInsets.all(0.0),
      onPressed: onPressed,
      child: Container(
        height: 60.0,
        width: 130.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(60.0),
        ),
        child: Text(
          title.toUpperCase(),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            letterSpacing: 2.0,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }
}
