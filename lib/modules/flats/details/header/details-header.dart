import 'package:easy_rent/modules/flats/common/carousel/carousel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum AppBarBehavior { normal, pinned, floating, snapping }

class CardDetailsHeader extends StatelessWidget {
  // Mutually exclusive things: requirements for constructor
  // and setting properties by default values for class
  // ignore: prefer_const_constructors_in_immutables
  CardDetailsHeader({this.width, this.images});

  final double _appBarHeight = 256.0;
  final AppBarBehavior _appBarBehavior = AppBarBehavior.pinned;
  final Animation<double> width;
  final List<DecorationImage> images;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      elevation: 0.0,
      forceElevated: true,
      leading: IconButton(
        key: const Key('GoBack'),
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
          size: 30.0,
        ),
      ),
      expandedHeight: _appBarHeight,
      pinned: _appBarBehavior == AppBarBehavior.pinned,
      floating:
      _appBarBehavior == AppBarBehavior.floating || _appBarBehavior == AppBarBehavior.snapping,
      snap: _appBarBehavior == AppBarBehavior.snapping,
      flexibleSpace: FlexibleSpaceBar(
        title: const Text('Студия'),
        background: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              width: width.value,
              height: _appBarHeight,
              child: Carousel(images: images)
            ),
          ],
        ),
      ),
    );
  }
}
