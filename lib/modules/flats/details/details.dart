import 'package:easy_rent/modules/flats/common/btn/card-btn.dart';
import 'package:easy_rent/modules/flats/common/frosted-container/frosted-container.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/material.dart';
import 'package:easy_rent/common/theme.dart';
import '../../../helpers/helpers.dart';
import 'content/details-content.dart';
import 'header/details-header.dart';

class Details extends StatefulWidget {
  const Details({
    Key key,
    this.images,
    this.onDislikePress,
    this.onLikePress,
  }) : super(key: key);

  final List<DecorationImage> images;
  final Function onLikePress;
  final Function onDislikePress;

  @override
  _DetailsState createState() => _DetailsState(images: images);
}

class _DetailsState extends State<Details> with TickerProviderStateMixin {
  _DetailsState({this.images});

  AnimationController _animationController;

  Animation<double> width;
  Animation<double> height;
  List<DecorationImage> images;

  @override
  void initState() {
    _animationController = AnimationController(duration: Duration(milliseconds: 2000), vsync: this);

    super.initState();

    width = Tween<double>(begin: 200.0, end: 220.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.ease),
    );

    height = Tween<double>(begin: 400.0, end: 400.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.ease),
    );
    height.addListener(() {
      setState(noop);
    });

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = .7;

    return Theme(
      data: ThemeData(
        brightness: Brightness.light,
        primaryColor: AppTheme.backgroundColor,
        fontFamily: AppTheme.fontFamily,
        platform: Theme.of(context).platform,
      ),
      child: Container(
        width: width.value,
        height: height.value,
        color: AppTheme.primaryColor,
        child: Hero(
          tag: 'img',
          child: Card(
            margin: const EdgeInsets.all(0.0),
            color: AppTheme.primaryColor,
            child: Container(
              alignment: Alignment.center,
              child: Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: <Widget>[
                  CustomScrollView(
                    shrinkWrap: false,
                    slivers: <Widget>[
                      CardDetailsHeader(width: width, images: images),
                      CardDetailsContent(),
                    ],
                  ),
                  Positioned(
                    bottom: 0.0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: FrostedContainer(
                        child: Container(
                          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              CardBtn(
                                title: 'Нет',
                                color: AppTheme.dislikeColor,
                                onPressed: widget.onDislikePress,
                              ),
                              CardBtn(
                                title: 'Да',
                                color: AppTheme.likeColor,
                                onPressed: widget.onLikePress,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
