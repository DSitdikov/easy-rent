import 'package:easy_rent/common/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CardDetailsContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(<Widget>[
        Container(
          color: AppTheme.primaryColor,
          child: Padding(
            padding: const EdgeInsets.all(35.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: AppTheme.primaryColor,
                    border: Border(
                      bottom: BorderSide(color: Colors.black12),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.attach_money,
                            color: AppTheme.accentColor,
                          ),
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              '15 т. руб. без КУ',
                              style: TextStyle(fontSize: 16.0),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                const Padding(padding: EdgeInsets.only(top: 16.0, bottom: 8.0)),
                const Text(
                  'Сдается впервые в аренду на длительный срок, светлая и просторная 1-но комнатная видовая квартира общей площадью (47м2) в в Василеостровском районе. Квартира находится в новом монолитном доме комфорт класса. В квартире выполнен современный и качественный евро ремонт, вся мебель и техника новые, квартира полностью укомплектована всем необходимым для комфортного проживания, есть аналоговое и цифровое TV, Wi-fi. Возле дома находится много разнообразной полезной и развитой инфраструктуры. Заезд на КАД и ЗСД. Аренда без домашних животных.',
                  style: TextStyle(fontSize: 16.0),
                ),
                Container(
                  height: 100.0,
                )
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
