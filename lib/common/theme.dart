import 'dart:ui';

class AppTheme {
  static const Color backgroundColor = Color(0xFF0B655A);
  static const Color primaryColor = Color(0xFFFFFFFF);
  static const Color secondaryColor = Color(0xffdfe6e9);
  static const Color accentColor = Color(0xFFFF8A63);

  static const Color likeColor = AppTheme.accentColor;
  static const Color dislikeColor = Color(0xFF636e72);
  static const Color primaryIconsColor = primaryColor;

  static const String fontFamily = 'Open Sans';
}
