import 'package:easy_rent/modules/flats/flats.dart';
import 'package:flutter/material.dart';

import 'common/theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final String appName = 'EasyRent';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      theme: ThemeData(
        fontFamily: AppTheme.fontFamily,
      ),
      home: FlatsModule(title: appName),
    );
  }
}
