# EasyRent

Welcome!

This is a [Tinder](https://ru.wikipedia.org/wiki/Tinder) analogue for renting flats.
The target goal: make process of renting for comfortable for people who rent and more safe 
for people who gives flats for renting.

## Getting Started

For help getting started with Flutter go here:
[documentation](https://flutter.io/)

## Useful content

1. [Resolving issues](doc/resolving-issues.md)
2. [Terms](doc/terms.md)
3. [Tests](doc/tests.md)

## Experience

1. Why we use stateless widgets instead of functional widgets? \
[Go there](https://stackoverflow.com/questions/53234825/what-is-the-difference-between-functions-and-classes-to-create-widgets/53234826?source=post_page---------------------------#53234826) \
**TODO:** [May be use functional widget](https://pub.dev/packages/functional_widget)

## Running tests

### E2E Tests example

```
flutter drive --target=test_driver/app.dart
```

### Unit widget tests example

Update "golden" (etalons / references) images:
```
flutter test --update-goldens
```

Run tests:
```
flutter test
```

For reading more about [tests](./doc/tests.md).

## Building

### Android
```
flutter build apk --split-per-abi
```

This command results in two APK files:
```
build/app/outputs/apk/release/app-armeabi-v7a-release.apk
build/app/outputs/apk/release/app-arm64-v8a-release.apk
```

Removing the --split-per-abi flag results in a fat APK that contains your code compiled 
for all the target [ABIs](https://ru.wikipedia.org/wiki/Двоичный_интерфейс_приложений). 
Such APKs are larger in size than their split counterparts, causing the user to download native binaries 
that are not applicable to their device’s architecture.

More about it building please read [here](https://flutter.dev/docs/deployment/android).

### iOS

Requires Apple Developer Account.
About iOS building please read [here](https://flutter.dev/docs/deployment/ios).

## Road Map for business-product

1. Fix updating state
2. List of liked flats
3. Support registration for rent agent 
4. Add optional support of registration of receiving feedbacks 

## Road Map for technical-product

1. Refactor bad code
2. Analyze and try some architecture
   Comparison provided [here](https://www.didierboelens.com/2019/04/bloc---scopedmodel---redux---comparison/) \
   [Great video by Google about BloC architecture](https://www.youtube.com/watch?v=RS36gBEp8OI) |
   [Another video about arch](https://www.youtube.com/watch?v=d_m5csmrf7I)
   
## Additional links

1. [Official app with examples of Flutter features](https://github.com/flutter/flutter/tree/master/examples/flutter_gallery)
2. [Dart for JS-developers](https://fireship.io/lessons/flutter-for-js-developers/#dart-vs-js-syntax)
3. [Flutter vs Swift comparison](https://blog.codemagic.io/flutter-vs-swift/)
4. [Creating for tables](https://iirokrankka.com/2018/01/28/implementing-adaptive-master-detail-layouts/)
