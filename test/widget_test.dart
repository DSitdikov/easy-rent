import 'package:easy_rent/common/theme.dart';
import 'package:easy_rent/modules/flats/common/btn/card-btn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('should correctly paint cart button', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        theme: ThemeData(
          fontFamily: AppTheme.fontFamily,
        ),
        home: Scaffold(
          body: const RepaintBoundary(
            key: Key('target'),
            child: CardBtn(title: 'Ok', color: AppTheme.accentColor),
          ),
        ),
      ),
    );

    await tester.pumpAndSettle();

    await expectLater(
      find.byKey(const Key('target')),
      matchesGoldenFile('../screenshots/widgets/card_btn/default.png'),
    );
  });
}
