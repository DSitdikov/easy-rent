import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Easy Rent Common', () {
    const String targetDir = 'screenshots/app/';

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      Directory(targetDir).create();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    Future<void> takeScreenshot(FlutterDriver driver, String path) async {
      final List<int> pixels = await driver.screenshot();
      final File file = File(path);
      await file.writeAsBytes(pixels);
    }

    test('should show stack of cards', () async {
      await takeScreenshot(driver, '${targetDir}shoud_show_stack_of_cards.png');
    });

    test('should remove card from stack on tap yes', () async {
      final SerializableFinder cardYesBtnFinder = find.byValueKey('YesButton 3');
      await driver.tap(cardYesBtnFinder);
      await takeScreenshot(driver, '${targetDir}should_remove_card-from_stack_on_tap_yes.png');
    });

    test('should open full description of second current card on card tapped', () async {
      final SerializableFinder card = find.byValueKey('DetailsLink');
      await driver.tap(card);
      await takeScreenshot(driver, '${targetDir}should_open_full_description_on_card_tapped.png');
    });

    test('should go back when back icon has been pressed', () async {
      final SerializableFinder goBack = find.byValueKey('GoBack');
      await driver.tap(goBack);
      await takeScreenshot(driver, '${targetDir}should_go_back_if_back_icon_has_been_pressed.png');
    });
  });
}
